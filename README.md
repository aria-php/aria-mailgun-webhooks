# ARIA Mailgun Webhook library

This is a wrapper / helper library for parsing mailgun webhook events. 

Just a stub for now, but will likely be built on.

This is an ARIA wrapper around both the legacy PHPMailer, and Swiftmailer.

## Installation

`composer require aria-php/aria-mailgun-webhooks` from within your project.

## Usage

Creating the parser:

```
use ARIA\mailgun\webhooks\Parser;

$parser = Parser::parser($api_key);

```

Parsing out an event

```

$event = $parser->parse($_POST);

```

Signatures will be validated at this point, and an exception thrown if things don't match up.
