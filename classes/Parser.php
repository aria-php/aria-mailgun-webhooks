<?php

namespace ARIA\mailgun\webhooks;

class Parser {
  
  private static $parser;
  
  private $key;

  public function __construct(string $api_key = '') {
    $this->setKey( $api_key );
    
    return $this;
  }
  
  public function getKey() : string {
    return $this->key;
  }
  
  public function setKey(string $api_key) {
    $this->key = $api_key;
  }
  
  public function parse(array $event): ?Event 
  {
    return new Event($event);
  }

  public static function parser(string $api_key = '') : Parser {
    
    if (empty(self::$parser)) {
      self::$parser = new Parser($api_key);
    }
    
    return self::$parser;
  }
  
}
