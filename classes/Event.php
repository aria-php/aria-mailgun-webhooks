<?php

namespace ARIA\mailgun\webhooks;

class Event {

  private $_signature = [];
  private $_payload = [];

  public function __construct(array $data) {

    if ($this->validateEventPayload($data)) {

      $this->_signature = $data['signature'];
      $this->_payload = $data['event-data'];

      if (!$this->validateSignature($this->_signature)) {
        throw new WebhookException("Signature is not valid");
      }
    }
  }

  protected function validateEventPayload(array $data): bool {

    if (empty($data)) {
      throw new WebhookException("Webhook payload is empty");
    }

    if (empty($data['signature'])) {
      throw new WebhookException("Webhook signature is missing");
    }

    if (empty($data['event-data'])) {
      throw new WebhookException("Webhook event data is missing");
    }

    return true;
  }

  protected function validateSignature(array $signature): bool {

    if (empty($signature['timestamp']) || empty($signature['token']) || empty($signature['signature'])) {
      throw new WebhookException("Webhook signature is missing fields");
    }

    $hmac = hash_hmac('sha256', $signature['timestamp'] . $signature['token'], Parser::parser()->getKey());

    return hash_equals($hmac, $signature['signature']);
  }

  public function getEventType(): ?string 
  {
    return $this->event;
  }
  
  public function getRecipient() : ? string 
  {
    return $this->recipient;
  }

  public function &__get($name) 
  {
    if (!isset($this->_payload[$name])) 
    {
      $this->_payload[$name] = null;
    }

    return $this->_payload[$name];
  }

  public function __set($name, $value) 
  {
    $this->_payload[$name] = $value;
  }

  public function __isset($name) 
  {
    if (!empty($this->_payload[$name])) 
    {
      return true;
    }
    
    return false;
  }

}
