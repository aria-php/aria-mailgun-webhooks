<?php

use ARIA\mailgun\webhooks\{ Parser, Event };

class ParserTest extends \PHPUnit\Framework\TestCase
{
  
  private $parser; 
  
  public function setUp(): void {
    
    if (defined('MAILGUN_KEY')) {
      $key = MAILGUN_KEY;
    }
    
    if (empty($key)) {
      $key = getenv('MAILGUN_KEY');
    }
    
    if (empty($key)) {
      throw new \RuntimeException("Missing MAILGUN_KEY, either define it or set an environment variable");
    }
    
    $this->parser = Parser::parser($key);
    
  }
  
  
  public function eventProvider() {
    
    $return = [];
    
    foreach ([
        'bounce'
    ] as $json ) {
      
      $return[$json] = [ json_decode(file_get_contents(__DIR__ . "/data/{$json}.json"), true) ];
      
    }
    
    return $return;
    
  }
  
  /**
   * @dataProvider eventProvider
   * @param array $data
   */
  public function testBasicParsing( array $event ) {
    
    $parser = Parser::parser();
    
    $event = $parser->parse($event);
    
    $this->assertNotEmpty($event->getEventType());
    $this->assertNotEmpty($event->getRecipient());
    $this->assertNotEmpty(filter_var($event->getRecipient(), FILTER_VALIDATE_EMAIL));
    
  }
}